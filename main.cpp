#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <wait.h>
#include <list>
#include <sys/ipc.h>
#include <sys/shm.h>

//Struct for the node of the queue
struct node
{
};

struct queue
{
	queue(size_t size)
	{
		key_t key = getpid();
		int shmid = shmget(key, (size+1)*sizeof(int), 0666|IPC_CREAT|IPC_EXCL);
		pids = (pid_t*)shmat(shmid, NULL, 0);
		pids[size] = 0;
	}

	size_t size;
	pid_t* pids;
};

void enqueue(struct queue& q, pid_t pid){
}
int dequeue(struct queue& q){
	return -1;
}

int g_child_count = 0;
//Signal hanlder,the termination of a process
void termchild(int signum){
	signal(SIGCHLD, termchild);
	printf("A child is dead\n");
	if (0 >= --g_child_count)
	{
		printf("All of my children died so bye...\n");
		exit(0);
	}		
}

int main(int argc,char const* argv[])
{
	signal(SIGCHLD, termchild);

	g_child_count = argc - 2;
	queue q (g_child_count);

	//Variable initialization
	int qt;
	if(argc>2)
	{
		qt = atoi(&argv[1][0]);//Here you need the qt
	}
	else
	{
		printf("%s qt prog1[prog2]...[progN]\n",argv[0]);
		exit(-1);
	}

	for(int i = 2; i < argc; ++i)
	{
		//Fork and execl processes
		pid_t pid = fork();
		// chld process
		if (pid == 0)
		{
			q.pids[i-2] = getpid();
			execl(argv[i], argv[i], 0, (char*)0);
			perror("execl() failure!\n");
			exit(1);
		}
	}

	sleep(1);//Small safe dealy

	int i = 0;
	pid_t process;
	while(true)//Scheduling loop
	{
		process = q.pids[i];
		if (process)
		{
			// resume
			if (0 == kill(process, SIGCONT))
			{
				// wait
				if (0 == kill(process, 0))
					sleep(qt);
				// sleep child process
				kill(process, SIGUSR1);	
			}
		}
		i =	(i + 1)%(argc-2);
	}
	shmdt(q.pids);
}

