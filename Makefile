CC=gcc
CFLAGS=-Wall -g
LDFLAGS=-Wall
SOURCES=main.cpp
EXECUTABLE=scheduler

.PHONY: all clean test

all: $(EXECUTABLE)
	
$(EXECUTABLE): $(SOURCES) 
	$(CC) $(CFLAGS) $< -o $@
	$(CC) $(CFLAGS) p.c -o p1
	$(CC) $(CFLAGS) p.c -o p3
	$(CC) $(CFLAGS) p.c -o p6
	$(CC) $(CFLAGS) p.c -o p9
	$(CC) $(CFLAGS) p.c -o p12

clean:
	rm $(EXECUTABLE) p1 p3 p6 p9 p12

test:
	./$(EXECUTABLE) 5 p1 p3 p6 p9 p12
